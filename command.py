import os


def install():
    os.system("git submodule init")
    os.system("git submodule update")
    os.system("mkdir ./MP-SPDZ/Player-Data")
    os.system(
        "sudo apt-get install automake build-essential git libboost-dev libboost-thread-dev libsodium-dev libssl-dev libtool m4 texinfo yasm -y"
    )
    os.system("pip install -r requirements.txt")
    os.system("./MP-SPDZ/Scripts/tldr.sh")
    os.system("cd MP-SPDZ && ls && make -j8 tldr")
    os.system("ln -s MP-SPDZ/Player-Data/ .")
    os.system("ln -s MP-SPDZ/local/ .")
    os.system("ln -s MP-SPDZ/Programs/ .")
    os.system("./MP-SPDZ/compile.py tutorial")
    os.system("cd MP-SPDZ && ls && make -j8 mascot")
    os.system("echo 1 2 3 4 > ./Player-Data/Input-P0-0")
    os.system("echo 1 2 3 4 > ./Player-Data/Input-P1-0")
    os.system("./MP-SPDZ/Scripts/mascot.sh tutorial")
    comp("xor")


def comp(a):
    os.system("cp ./mpc/" + a + ".mpc ./Programs/Source/")
    os.system("./MP-SPDZ/compile.py " + a)



if __name__ == "__main__":
    install()
