from connection.broadcast import BroadcastClient
from connection.socket import SocketClient
import time
import config as cfg
from .userParser import UserParser
import secrets
from Utils import *
import requests
from tls.decrypt import AES_GCM
from Utils import *
import threading

import sys
class User:
    def __init__(self, data, server, broadcast, broadcastPort):
        time.sleep(1)
        print("user")
        self.onlineOracles = []
        self.id = "PADRA"
        self.server = server
        self.data = data
        self.dataLen = len(data) // 2
        self.parser = UserParser(self)
        self.broadcast = BroadcastClient(
            broadcast=broadcast,
            broadcastPort=broadcastPort,
            parser=self.parser,
            id=self.id,
        )
        self.keyCryptor = secrets.token_hex(16)
        self.keyConnector = secrets.token_hex(16)
        self.cryData = None
        self.conData = None
        self.doneThread = threading.Thread(target=self.done)
    def run(self):
        self.broadcast.send(
            self.parser.requestOracleBroadcast(self.id), self.broadcast.addr
        )

        while len(self.onlineOracles) < cfg.numberOfOracles:
            l = len(self.onlineOracles)
            yBackPrint(
                "Warning",
                "We need min {cfg.numberOfOracles} Oracles. Waiting for mor Oracle. we have now {l} online Oracles",
            )
            time.sleep(0.5)
            self.broadcast.send(
                self.parser.requestOracleBroadcast(self.id), self.broadcast.addr
            )

        gBackPrint(
            "now we have min {cfg.numberOfOracles} Oracles. List of All Online Oracles",
            self.onlineOracles,
        )

        worker = []

        # Socket creation
        for i in range(cfg.numberOfOracles):
            worker.append(
                SocketClient(
                    "127.0.0.1", int(self.onlineOracles[i][1]), self.parser, None
                )
            )

        #  send rule to each oracle
        for i in range(cfg.numberOfOracles):
            data = self.parser.socketConnecting(i)
            worker[i].id = i
            worker[i].send(data)

        time.sleep(1)

        worker[0].send(
            self.parser.oracleInitialConnector(
                self.server,
                worker[1].Host,
                worker[2].Host,
                self.dataLen,
                self.keyConnector,
            )
        )

        worker[1].send(
            self.parser.oracleInitialCryptor(
                (worker[0].Host, worker[0].port + 1),
                self.server,
                self.dataLen,
                self.keyCryptor,
            )
        )

        worker[2].send(
            self.parser.oracleInitialBearer(
                (worker[0].Host, worker[0].port + 1), self.data
            )
        )


    def done(self):
        while not( self.conData and self.cryData):
            time.sleep(1)

        cry = self.parser.toJSON(self.cryData)
        key= cry["key"]
        iv= cry["iv"]
        con = self.parser.toJSON(self.conData)
        cipher= con["cipher"]

        dec = AES_GCM(int(key, 16),int(iv, 16))
        p = dec.decrypt(bytes.fromhex(cipher))
        gPrint("DATA IS",p)
        sys.exit()
        

    def cryptorDone(self,data):
        gPrint("cryptorDone", data["data"])
        d = data["data"]
        r = requests.get(d)
        self.cryData = self.parser.decrypt(self.keyCryptor,r.text)
        # self.cryData = r.text
        print(self.cryData)
        
        self.doneThread.start()



    def connectorDone(self,data):
        gPrint("connectorDone",  data["data"])
        d = data["data"]
        r = requests.get(d)
        self.conData = self.parser.decrypt(self.keyConnector,r.text)
        # self.conData = r.text
        print(self.conData)



# TODO response from Oracle in decrypt with 
""" if __name__ == '__main__':

    master_key = 0xb9b8887ee 6e1e8b78e2aaa2a0c522c13
    iv = 0xacf448fa5d3c87b08f549c5e
    data = b"\x00"
    my_gcm = AES_GCM(master_key, iv)
    my_gcm.decrypt(data)
 """