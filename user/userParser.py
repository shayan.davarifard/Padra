from connection.parser import Parser
from Utils import *


class UserParser(Parser):
    def __init__(self, user):
        self.user = user
        print("createt User Parser")


    def parser(self, data, addr, Connection):
        try:
            if data["type"] == "responseUserBroadcast":
                self.responseUserBroadcast(data, addr)
            if data["type"] == "connectorDone":
                self.user.connectorDone(data)
            if data["type"] == "cryptorDone":
                self.user.cryptorDone(data)
        except Exception as e:
            rBackPrint("An exception occurred parser", e)

    def requestOracleBroadcast(self, id):
        try:
            data = {}
            self.add(data, "type", "requestOracleBroadcast")
            self.add(data, "id", id)
            return self.toSEND(data)
        except Exception as e:
            rBackPrint("An exception occurred requestOracleBroadcast", e)

    def responseUserBroadcast(self, data, addr):
        try:
            self.user.onlineOracles.append((addr[0], data["socket"]))
        except Exception as e:
            rBackPrint("An exception occurred responseUserBroadcast", e)

    def socketConnecting(self, oracleNumber):
        try:
            data = {}
            self.add(data, "type", "socketConnecting")
            self.add(data, "oracle", oracleNumber)
            return self.toSEND(data)
        except Exception as e:
            rBackPrint("An exception occurred socketConnecting", e)

    def oracleInitialConnector(self, server, cryptor, bearer, dataLen, key):
        try:
            data = {}
            self.add(data, "type", "oracleInitial")
            self.add(data, "server", server)
            self.add(data, "bearer", bearer)
            self.add(data, "cryptor", cryptor)
            self.add(data, "dataLen", dataLen)
            self.add(data, "key", key)
            return self.toSEND(data)
        except Exception as e:
            rBackPrint("An exception occurred oracleInitialConnector", e)

    def oracleInitialCryptor(self, connector, server, dataLen, key):
        try:
            data = {}
            self.add(data, "type", "oracleInitial")
            self.add(data, "connector", connector)
            self.add(data, "server", server)
            self.add(data, "dataLen", dataLen)
            self.add(data, "key", key)
            return self.toSEND(data)
        except Exception as e:
            rBackPrint("An exception occurred oracleInitialCryptor", e)

    def oracleInitialBearer(self, connector, d):
        try:
            data = {}
            self.add(data, "type", "oracleInitial")
            self.add(data, "connector", connector)
            self.add(data, "data", d)
            return self.toSEND(data)
        except Exception as e:
            rBackPrint("An exception occurred oracleInitialBearer", e)
