CEND = "\33[0m"
CBOLD = "\33[1m"
CITALIC = "\33[3m"
CURL = "\33[4m"
CBLINK = "\33[5m"
CBLINK2 = "\33[6m"

CSELECTED = "\33[7m"
CBLACKBG = "\33[40m"
CREDBG = "\33[41m"
CGREENBG = "\33[42m"
CYELLOWBG = "\33[43m"
CBLUEBG = "\33[44m"
CVIOLETBG = "\33[45m"
CBEIGEBG = "\33[46m"
CWHITEBG = "\33[47m"

CGREY = "\33[90m"
CRED2 = "\33[91m"
CGREEN2 = "\33[92m"
CYELLOW2 = "\33[93m"
CBLUE2 = "\33[94m"
CVIOLET2 = "\33[95m"
CBEIGE2 = "\33[96m"
CWHITE2 = "\33[97m"


def simplePrint(a, b):
    print("\n", CWHITE2, "\t", a, " -> ", b, CEND, "\n")


def yBackPrint(a, b):
    print("\n", CYELLOWBG, "\t", a, " -> ", b, CEND, "\n")


def gBackPrint(a, b):
    print("\n", CGREENBG, "\t", a, " -> ", b, CEND, "\n")


def rBackPrint(a, b):
    print("\n", CREDBG, "\t", a, " -> ", b, CEND, "\n")


def bBackPrint(a, b):
    print("\n", CBLUEBG, "\t", a, " -> ", b, CEND, "\n")


def vBackPrint(a, b):
    print("\n", CVIOLETBG, "\t", a, " -> ", b, CEND, "\n")


def yPrint(a, b):
    print("\n", CYELLOW2, "\t", a, " -> ", b, CEND, "\n")


def gPrint(a, b):
    print("\n", CGREEN2, "\t", a, " -> ", b, CEND, "\n")


def rPrint(a, b):
    print("\n", CRED2, "\t", a, " -> ", b, CEND, "\n")


def bPrint(a, b):
    print("\n", CBLUE2, "\t", a, " -> ", b, CEND, "\n")


def vPrint(a, b):
    print("\n", CVIOLET2, "\t", a, " -> ", b, CEND, "\n")


def spacePrint():
    print("\n\t*************************\n")
