from optparse import OptionParser
from oracle.oracle import Oracle
from user.user import User
from command import install, comp
import os
from Utils import *


def main():
    os.system("clear")
    parser = OptionParser()

    parser.add_option(
        "-i",
        "--installation",
        action="store_true",
        dest="installation",
        default=False,
        help="installation",
    )

    parser.add_option(
        "-o",
        "--oracle",
        action="store_true",
        dest="oracle",
        default=False,
        help="if true -> orcle will be run",
    )

    parser.add_option(
        "-u",
        "--user",
        action="store_true",
        dest="user",
        default=False,
        help="if true -> user will be run",
    )

    parser.add_option(
        "-s",
        "--server",
        dest="server",
        default="api.spacexdata.com",
        help="destination Server for feed data to Oracles",
    )

    parser.add_option(
        "-d",
        "--data",
        dest="data",
        default="474554202f76332f736869707320485454502f312e310d0a557365722d4167656e743a20576765742f312e32302e3320286d696e67773332290d0a4163636570743a202a2f2a0d0a4163636570742d456e636f64696e673a206964656e746974790d0a486f73743a206170692e737061636578646174612e636f6d0d0a436f6e6e656374696f6e3a204b6565702d416c6976650d0a0d0a17",
        help="Data to send to destination server for response",
    )

    parser.add_option(
        "-b",
        "--broadcast",
        dest="broadcast",
        default="255.255.255.255",
        help="Broad cast IP",
    )

    parser.add_option(
        "-p",
        "--broadcast-port",
        dest="broadcastPort",
        default=6666,
        help="Broad cast Port",
    )

    parser.add_option("-c", "--compile", dest="compile", help="file compile")

    (options, args) = parser.parse_args()

    if len(args) >= 1 or (
        options.oracle + options.user != 1
        and not options.installation
        and not options.compile
    ):
        rBackPrint(
            "ERROR",
            "\n\n\tWhat do you want to run? Oracle or User? \n\n\tplease use -o OR -u. \n\n\tFor more Info you can use -h or --help or i can do it for you\n \n\t~~~ GOOD LUCK :) ~~~\n",
        )
        parser.print_help()
        return

    if options.compile:
        comp(options.compile)

    elif options.installation:
        install()

    elif options.oracle:
        Oracle(broadcast=options.broadcast,
               broadcastPort=options.broadcastPort)

    elif options.user:
        User(
            data=options.data,
            server=options.server,
            broadcast=options.broadcast,
            broadcastPort=options.broadcastPort,
        ).run()


if __name__ == "__main__":
    main()
