import socket
import threading
import config as cfg
from Utils import *

LenFunc = cfg.LenFunc


class SocketHTTP(threading.Thread):
    def __init__(self, HOST, PORT, parent):
        try:
            self.parent = parent
            super(SocketHTTP, self).__init__(None, None, None, None)
            self.port = PORT
            self.Host = HOST
            self.history = []
            self.socket = socket.socket()
            self.socket.connect((HOST, PORT))
            t = threading.Thread(target=self.receive)
            t.start()
            gBackPrint(
                "socket is created and Listener is created", (HOST, PORT))
        except Exception as e:
            rBackPrint("An exception occurred SocketHTTP __init__", e)

    def close(self):
        try:
            self.socket.close()
            vPrint("SocketHTTP", " CLOSED")
        except Exception as e:
            rBackPrint("An exception occurred SocketHTTP close", e)

    def send(self, M):
        try:
            self.socket.sendall(M)
            self.history.append(("send", M))
            vPrint("SocketHTTP Send", M)
        except Exception as e:
            rBackPrint("An exception occurred SocketHTTP send", e)

    def receive(self):
        try:
            while True:
                data = self.socket.recv(65565)
                bPrint("receive", data)
                if bytes.hex(data) == b"":
                    self.close
                    break
                self.parent.recv(bytes.hex(data))
        except Exception as e:
            rBackPrint("An exception occurred SocketHTTP receive", e)


class SocketClient(threading.Thread):
    def __init__(self, HOST, PORT, parser, id):
        try:
            self.parser = parser
            super(SocketClient, self).__init__(None, None, None, None)
            self.port = PORT
            self.Host = HOST
            self.history = []
            self.id = id
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.socket.connect((HOST, PORT))
            t = threading.Thread(target=self.receive)
            t.start()
            gPrint("socket is created", " Listener is created")
        except Exception as e:
            rBackPrint("An exception occurred SocketClient __init__", e)

    def send(self, M):
        try:
            Message = bytes(M, "utf-8")
            vPrint("Befor sendSocketHTTP Send", Message)
            self.socket.sendall(Message)
            # self.history.append(("send", Message))
            vPrint("SocketHTTP Send", M)
        except Exception as e:
            rBackPrint("An exception occurred SocketClient send", e)

    def receive(self):
        try:
            while True:
                len = self.socket.recv(cfg.LenFunc)
                if not len:
                    break
                data = self.socket.recv(int(len.decode("utf-8")))
                data = self.parser.toJSON(data.decode("utf-8"))
                bPrint("receive", data)
                self.parser.parser(data, None, self)
        except Exception as e:
            rBackPrint("An exception occurred SocketClient receive", e)


class SocketServer:
    def __init__(self, socketPort, parser, id):
        try:
            self.parser = parser
            self.history = []
            self.connected = []
            self.socketPort = socketPort
            self.id = id
            t = threading.Thread(target=self.connection)
            t.start()
            gPrint("socket is created and Listener is created", socketPort)
        except Exception as e:
            rBackPrint("An exception occurred SocketServer __init__", e)

    def connection(self):
        try:
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                s.bind(("localhost", self.socketPort))
                s.listen()
                self.socket = s
                while True:
                    conn, addr = self.socket.accept()
                    print("user connected to ", conn, "-> ", addr)
                    u = Connection(
                        conn, addr, self.socketPort + 1, self.parser)
                    self.connected.append(u)
        except Exception as e:
            rBackPrint("An exception occurred SocketServer connection", e)


class Connection:
    def __init__(self, connection, address, oracleSocketPort, parser):
        try:
            self.parser = parser
            self.socket = connection
            self.addr = address
            self.history = []
            self.type = None
            self.oracleSocketPort = oracleSocketPort
            t = threading.Thread(target=self.receive)
            t.start()
        except Exception as e:
            rBackPrint("An exception occurred Connection __init__", e)

    def send(self, M):
        try:
            Message = bytes(M, "utf-8")
            self.socket.sendall(Message)
            self.history.append(("send", Message))
            vPrint("Connection Send", M)
        except Exception as e:
            rBackPrint("An exception occurred Connection send", e)

    def receive(self):
        try:
            while True:
                len = self.socket.recv(cfg.LenFunc)
                if not len:
                    break
                data = self.socket.recv(int(len.decode("utf-8")))
                data = self.parser.toJSON(data.decode("utf-8"))
                bPrint("Connection receive", data)
                self.parser.parser(data, None, self)
        except Exception as e:
            rBackPrint("An exception occurred Connection receive", e)
