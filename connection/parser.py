import json
import config as cfg
from Utils import *
from tls.AES import AESCipher
LenFunc = cfg.LenFunc


class Parser:
    def __init__(self):
        gBackPrint("parser ", "created")

    def toJSON(self, data):
        return json.loads(data)

    def toSTRING(self, data):
        return json.dumps(data)

    def toSEND(self, data):
        j = json.dumps(data)
        l = LenFunc - len(str(len(j)))
        return "00000"[0:l] + str(len(j)) + j

    def add(self, data, k, v):
        try:
            data[k] = v
            return data
        except Exception as e:
            rBackPrint("An exception occurred add", e)

    def parser(self, data, addr, id):
        data = self.toJSON(data)
        gPrint("Simple parser", data)

    def encrypt(self,key, msg):
        return AESCipher(key).encrypt(msg)

    def decrypt(self,key, msg):
        return AESCipher(key).decrypt(msg)