import json
import os
import socket
import threading
import config as cfg
from nacl.public import PrivateKey
from connection.socket import SocketClient
from tls import TLSClientSession
import time
from Crypto.Util.number import long_to_bytes, bytes_to_long
from tls.ghash import GhashCry
from Utils import *
from Crypto.Cipher import AES
from smartcontract.script import main
import sys

def new_x25519():
    private_key = PrivateKey.generate()
    key_exchange = bytes(private_key.public_key)
    return bytes(private_key).hex(), key_exchange.hex()


class Cryptor:
    def __init__(self, parser):
        self.privatekey, self.key_exchange = new_x25519()
        self.server = None
        self.parser = parser
        self.connector = None
        self.key = None
        
        self.doneThread = threading.Thread(target=self.done)
        gBackPrint("CRYPTOR", "")
        gPrint("privatekey", self.privatekey)
        gPrint("key_exchange", self.key_exchange)

    def oracleInitial(self, data):
        try:
            self.server = data["server"]
            self.connector = SocketClient(
                data["connector"][0], data["connector"][1], self.parser, "Cryptor"
            )
            self.key = data["key"]
            dataLen = data["dataLen"]
            if 0 == dataLen % 16:
                datalen = dataLen // 16
            else:
                datalen = (dataLen + (16 - dataLen % 16)) // 16
            self.dataLen = datalen
            time.sleep(1)
            self.keyExchange()
        except Exception as e:
            rBackPrint("An exception occurred oracleInitial", e)

    def keyExchange(self):
        try:
            d = {}
            self.parser.add(d, "type", "keyExchange")
            self.parser.add(d, "keyExchange", self.key_exchange)
            self.connector.send(self.parser.toSEND(d))
        except Exception as e:
            rBackPrint("An exception occurred keyExchange", e)

    def fromContoCryHelloCLient(self, data):
        try:
            self.tls = TLSClientSession(
                bytes.fromhex(self.privatekey),
                bytes.fromhex(self.key_exchange),
                bytearray(data["data"], "utf-8"),
                self.fromCrytoCon,
                self.fromCrytoConDone,
            )
        except Exception as e:
            rBackPrint("An exception occurred fromContoCryHelloCLient", e)

    def fromCrytoCon(self, data):
        try:
            self.parser.parserSend(data, self.connector, "fromCrytoCon")
        except Exception as e:
            rBackPrint("An exception occurred fromCrytoCon", e)

    def fromCrytoConDone(self, change, finish):
        try:
            bPrint("Start to send Done to Connection ", self.tls.cipher.key)
            n = self.tls.cipher.get_nonce()
            k = self.tls.cipher.key
            bPrint("Key", k)
            bPrint("Nonce", n)
            g = GhashCry(bytes_to_long(k), bytes_to_long(n))
            f, l = g.get()
            d = {}
            self.parser.add(d, "type", "fromCrytoConDone")
            self.parser.add(d, "change", change)
            self.parser.add(d, "finish", finish)
            self.parser.add(d, "auth_key", f)
            self.parser.add(d, "last_key", l)
            self.counter = g.encIV(self.dataLen)
            t = b""
            for x in self.counter:
                t = t + x
            self.counter2 = t
            self.connector.send(self.parser.toSEND(d))
            self.doneThread.start()


        except Exception as e:
            rBackPrint("An exception occurred fromCrytoConDone", e)
    def done(self):
            d = {}

            self.parser.add(d,"key",self.tls.peer_cipher.key.hex())
            self.parser.add(d,"iv",self.tls.peer_cipher.ivhex.hex())

            res = self.parser.encrypt(self.key,json.dumps(d))
            # res = json.dumps(d)
            print(res)
            print(d)


            file_path= "./Player-Data/cryptor.txt"
            f = open(file_path, "w")
            f.write(res)
            f.close()
            data_to_user = main(file_path)

            self.parser.parserSend(data_to_user,self.parser.user,"cryptorDone")
            sys.exit()



    def MPC(self, json):
        try:
            n = int(json["data"])
            bPrint("MPC Input", self.counter2[4 * n : 4 * (1 + n)].hex())
            bPrint(
                "MPC Input int",
                str(bytes_to_long((self.counter2[4 * n : 4 * (1 + n)]))),
            )


            s = (
                "echo "
                + str(bytes_to_long((self.counter2[4 * n : 4 * (1 + n)])))
                + "  > ./Player-Data/Input-P2-0"
            )
            os.system(s)
            os.system(
                "./MP-SPDZ/mascot-party.x 2 xor  -pn 12023 -h 127.0.0.1 -N 3"
            )
        except Exception as e:
            rBackPrint("An exception occurred MPC", e)

    def fromContoCry(self, data):
        try:
            self.tls.tls_response(memoryview(bytes.fromhex(data["data"])))
        except Exception as e:
            rBackPrint("An exception occurred fromContoCry", e)
