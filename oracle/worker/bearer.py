import os
import socket
import time
import threading
import config as cfg
from connection.socket import SocketClient
from Crypto.Util.number import long_to_bytes, bytes_to_long
from Utils import *


class Bearer:
    def __init__(self, parser):
        self.parser = parser
        self.connector = None
        self.data = None
        gBackPrint("BEARER", "")

    def oracleInitial(self, data):
        try:
            time.sleep(3)
            self.connector = SocketClient(
                data["connector"][0], data["connector"][1], self.parser, "Bearer"
            )
            self.data = bytes.fromhex(data["data"])
            self.dataLen = len(self.data)
            bPrint("len", self.dataLen)
            if not self.dataLen % 16 == 0:
                bPrint("00", b"\x00" * (16 - self.dataLen % 16))
                self.data = self.data + b"\x00" * (16 - self.dataLen % 16)
            bPrint("len", self.dataLen)
        except Exception as e:
            rBackPrint("An exception occurred oracleInitial", e)

    def MPC(self, json):
        try:
            n = int(json["data"])

            bPrint("MPC Input", self.data[4 * n : 4 * (1 + n)].hex())
            bPrint(
                "MPC Input int", str(bytes_to_long((self.data[4 * n : 4 * (1 + n)])))
            )
            s = (
                "echo "
                + str(bytes_to_long((self.data[4 * n : 4 * (1 + n)])))
                + "  > ./Player-Data/Input-P1-0"
            )
            os.system(s)
            os.system(
                "./MP-SPDZ/mascot-party.x 1 xor  -pn 12023 -h 127.0.0.1 -N 3"
            )
        except Exception as e:
            rBackPrint("An exception occurred MPC", e)
