from connection.broadcast import BroadcastServer
from connection.socket import SocketServer
from .oracleParser import OracleParser
import random
import os


class Oracle:
    def __init__(self, broadcast, broadcastPort):

        self.id = os.urandom(32)
        self.socketPort = int(random.random() * 50000 + 1024)
        self.parser = OracleParser(self)  # ,socketPort=socketPort
        self.b = BroadcastServer(
            broadcastPort=broadcastPort, parser=self.parser, id=self.id
        )
        self.s = SocketServer(self.socketPort, parser=self.parser, id=self.id)
