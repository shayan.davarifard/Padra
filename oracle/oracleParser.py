from connection.parser import Parser
from .worker.cryptor import Cryptor
from .worker.connector import Connector
from .worker.bearer import Bearer
from Utils import *


class OracleParser(Parser):
    def __init__(self, oracle):
        self.socket = oracle.socketPort
        self.oracle = oracle
        self.type = None
        self.id = None
        self.user = None
    def parser(self, data, addr, Connection):
        try:
            if data["type"] == "requestOracleBroadcast":
                self.requestOracleBroadcast(data, addr)
            elif data["type"] == "socketConnecting":
                self.socketConnecting(data)
            elif data["type"] == "oracleInitial":
                self.user = Connection
                self.type.oracleInitial(data)
            elif data["type"] == "keyExchange":
                self.type.keyExchange(data)
            elif data["type"] == "fromContoCry":
                self.type.fromContoCry(data)
            elif data["type"] == "fromConMPC":
                self.type.MPC(data)
            elif data["type"] == "fromCrytoCon":
                self.type.fromCrytoCon(data)
            elif data["type"] == "fromContoCryHelloCLient":
                self.type.fromContoCryHelloCLient(data)
            elif data["type"] == "fromCrytoConDone":
                self.type.fromCrytoConDone(data)
            elif data["type"] == "fromContoCryDone":
                self.type.fromContoCryDone(data)
            elif data["type"] == "MPC":
                self.type.MPC(data)
        except Exception as e:
            rBackPrint("An exception occurred parser", e)

    def requestOracleBroadcast(self, data, addr):
        try:
            d = {}
            self.add(d, "type", "responseUserBroadcast")
            self.id = data["id"]
            self.add(d, "message", "Hello " + data["id"])
            self.add(d, "socket", self.socket)
            self.oracle.b.send(self.toSEND(d), addr)
        except Exception as e:
            rBackPrint("An exception occurred requestOracleBroadcast", e)

    def socketConnecting(self, data):
        try:
            if data["oracle"] == 0:
                self.type = Connector(self, self.socket + 1)
            elif data["oracle"] == 1:
                self.type = Cryptor(self)
            elif data["oracle"] == 2:
                self.type = Bearer(self)
            else:
                print("An exception occurred socketConnecting Oracle data ")
        except Exception as e:
            rBackPrint("An exception occurred socketConnecting", e)

    def parserSend(self, data, connection, typeO):
        try:
            d = {}
            self.add(d, "type", typeO)
            self.add(d, "data", data)
            connection.send(self.toSEND(d))
        except Exception as e:
            rBackPrint("An exception occurred parserSend", e)
