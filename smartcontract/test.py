from web3 import Web3
import json
import ipfshttpclient
import pprint
from web3.gas_strategies.time_based import medium_gas_price_strategy, fast_gas_price_strategy



#infura_url_mainnet = "https://mainnet.infura.io/v3/88df052c847f4d80b839a6cdd00a515c"
infura_kovan = "https://kovan.infura.io/v3/88df052c847f4d80b839a6cdd00a515c"
# ganache_url = "http://127.0.0.1:7545"



print("Connecting to Ethereum Test Network...")
web3 = Web3(Web3.HTTPProvider(infura_kovan))
# web3.eth.defaultAccount = web3.eth.accounts[0] #local
web3.eth.defaultAccount = "0x92582427Bc16dEE757a20265F34460E13Fb05409"
private_key = "c41170d8d93fb438f3a910d4a079d5e2140b55958c8f136b816c29280b2ed43d"
print(f"Connected to Blockchain Test Network? {web3.isConnected()}")
balance = web3.fromWei(web3.eth.getBalance(web3.eth.defaultAccount), 'ether')
balance2 = web3.fromWei(web3.eth.getBalance(web3.eth.defaultAccount), 'gwei')
print(balance)
print(balance2)
# web3.eth.setGasPriceStrategy(fast_gas_price_strategy)
# gasprice = web3.eth.generateGasPrice()
# gasprice2 = web3.fromWei(gasprice, 'gwei')
# print(" gas price in wei", gasprice)
# print(" gas price in gwei", gasprice2)

gas_estimate = 25000
gas_limit = gas_estimate + (gas_estimate * 0.1)
print (gas_limit)
# print(f'Gas estimate to transact with store function: {gas_estimate}')
# transaction_cost = gasprice2 * gas_estimate
# print(f'Transaction cost estimate: {transaction_cost} gwei')
# if (balance2 > transaction_cost):
#     print("transaction can be done")
# else:
#     print("not enough balance")