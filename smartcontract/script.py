from web3 import Web3
import json
import ipfshttpclient
import pprint
from web3.gas_strategies.time_based import medium_gas_price_strategy, fast_gas_price_strategy
from Utils import *

def main(file_path):
    #infura_url_mainnet = "https://mainnet.infura.io/v3/88df052c847f4d80b839a6cdd00a515c"
    infura_kovan = "https://kovan.infura.io/v3/88df052c847f4d80b839a6cdd00a515c"
    # ganache_url = "http://127.0.0.1:7545"



    simplePrint("Connecting "," IPFS...")
    ipfs_uri = "/dns/ipfs.infura.io/tcp/5001/https"
    client = ipfshttpclient.connect(ipfs_uri)

    result = client.add(file_path)
    #print(result['Hash'])
    #print(type(result))
    result_hash = str(result['Hash'])
    simplePrint("Response saved on IPFS with this hash", result_hash)


    simplePrint("Connecting "," Ethereum Test Network...")
    web3 = Web3(Web3.HTTPProvider(infura_kovan))
    # web3.eth.defaultAccount = web3.eth.accounts[0] #local
    web3.eth.defaultAccount = "0x92582427Bc16dEE757a20265F34460E13Fb05409"
    private_key = "c41170d8d93fb438f3a910d4a079d5e2140b55958c8f136b816c29280b2ed43d"
    bPrint("Connected to Blockchain Test Network?", web3.isConnected())
    balance = web3.fromWei(web3.eth.getBalance(web3.eth.defaultAccount), 'gwei')
    #print(web3.eth.blockNumber)
    #print(web3.eth.getTransactionCount(web3.eth.defaultAccount))

    # sm_address_local = web3.toChecksumAddress("0x35341508Df8D36806171e4c62E2Df30004C8C774")
    sm_address = web3.toChecksumAddress("0x77161194E4d4B5af4Ec330CcE2f4F4A98a068d60")
    sm_abi = json.loads('[{"constant":false,"inputs":[{"name":"hash","type":"string"}],"name":"store","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"retrieve","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"}]')

    contract = web3.eth.contract(address = sm_address, abi= sm_abi)
    #print(contract)

    # local env
    # tx_hash = contract.functions.store(result_hash).transact()
    # print (tx_hash)
    # print(f"Access data from ipfs after writing to smart contract: https://ipfs.infura.io/ipfs/{contract.functions.retrieve().call()}")
    # tx_receipt = web3.eth.waitForTransactionReceipt(tx_hash)
    # print(tx_receipt)

    # Ethernet testnet 
    gas_estimate = contract.functions.store(result_hash).estimateGas()
    bPrint('Gas estimate to transact with store function',gas_estimate)
    web3.eth.setGasPriceStrategy(fast_gas_price_strategy)
    gasprice = web3.fromWei(web3.eth.generateGasPrice(), 'gwei')
    transaction_cost = gasprice * gas_estimate
    gas_limit = gas_estimate + (gas_estimate * 0.1)
    bBackPrint('Transaction cost estimate',transaction_cost)
    if (balance > transaction_cost):
        gBackPrint("Enough balance"," processing...")
        gPrint("Sending transaction"," to smart contract")
        transaction = contract.functions.store(result_hash).buildTransaction()
        transaction.update({ 'gas' : gas_estimate })
        transaction.update({ 'nonce' : web3.eth.getTransactionCount(web3.eth.defaultAccount) })
        signed_tx = web3.eth.account.signTransaction(transaction, private_key)
        #print(signed_tx)
        txn_hash = web3.eth.sendRawTransaction(signed_tx.rawTransaction)
        txn_receipt = web3.eth.waitForTransactionReceipt(txn_hash)
        gBackPrint("Transaction receipt mined:","")
        pprint.pprint(dict(txn_receipt))
        if (txn_receipt["status"] == 1):
            gBackPrint("","Transaction was successful?")
            gBackPrint("Access data from ipfs after writing to smart contract: ",f"https://ipfs.infura.io/ipfs/{contract.functions.retrieve().call()}")
        else:
            rBackPrint(f"transaction was not successful")
            return None


        return f"https://ipfs.infura.io/ipfs/{contract.functions.retrieve().call()}"
    else:
        yBackPrint("Balance is not sufficent")
        return None