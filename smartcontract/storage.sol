pragma solidity >=0.4.22 <0.7.0; //compiled with 0.4.22

/**
 * @title Storage
 * @dev Store & retrieve value in a variable
 */
contract Storage {

    string hash_url;

    /**
     * @dev Store value in variable
     * @param hash value to store
     */
    function store(string hash) public {
        hash_url = hash;
    }

    /**
     * @dev Return value 
     * @return value of 'hash_url'
     */
    function retrieve() public view returns (string){
        return hash_url;
    }
}